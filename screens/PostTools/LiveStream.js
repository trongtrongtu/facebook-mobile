import PubNubReact from "pubnub-react";
import React, { Component } from "react";
import { StyleSheet } from "react-native";
import { GiftedChat } from "react-native-gifted-chat";
import io from 'socket.io-client';
import { AsyncStorage } from 'react-native';

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            messages: [],
            id: Math.random()
        };
        this.socket = null;
    }
    componentWillMount() {
        // this.setState({
        //     messages: [
        //         {
        //             _id: 1,
        //             text: "Hello developer",
        //             createdAt: new Date(),
        //             user: {
        //                 _id: 2,
        //                 name: "React Native",
        //                 avatar: "https://placeimg.com/140/140/any",
        //             },
        //         },
        //     ],
        // });
        this.socket = io('http://192.168.1.62:3001');
        this.socket.emit("login", { username: 'user_login', roomName: 'room' });
        this.socket.on('newMessageFriend', (response) => { this.setState({ message: [m, ...message] }) }); //lắng nghe khi có tin nhắn mới

        // listMessageWithUser(sessionStorage.getItem('user_login'), sessionStorage.getItem('user_friend')).then((listMessage) => {
        //     for (let i = 0; i < listMessage.length; i++) {
        //         this.state.messages.push({
        //             message: listMessage[i].message,
        //             userName: listMessage[i].username,
        //             timeM: listMessage[i].created_date,
        //             usernamefriend: listMessage[i].usernamefriend
        //         })
        //     }
        //     this.setState({ messages: this.state.messages })
        // })
    }
    newMessage = (m) => {
        const messages = this.state.messages;
        let ids = _map(messages, 'id');
        let max = Math.max(...ids);

        messages.push({
            message: m.data,
            userName: m.user.name,
            timeM: m.timeM,
            usernamefriend: m.usernamefriend
        });

        let objMessage = $('.chat-message');
        if (objMessage[0].scrollHeight - objMessage[0].scrollTop === objMessage[0].clientHeight) {
            this.setState({ messages });
            objMessage.animate({ scrollTop: objMessage.prop('scrollHeight') }, 1000); //tạo hiệu ứng cuộn khi có tin nhắn mới

        } else {
            this.setState({ messages });
            if (m.id === this.state.user) {
                objMessage.animate({ scrollTop: objMessage.prop('scrollHeight') }, 1000);
            }
        }

    }

    //Gửi event socket newMessage với dữ liệu là nội dung tin nhắn và người gửi
    sendnewMessage = (m, id) => {
        this.socket.emit("newMessageFriend", m); //gửi event về server
    }
    //login để định danh người dùng
    login() {

        this.socket.emit("login", this.refs.name.value);

    }

    join_room() {
        this.socket.emit("join-room", { roomName: this.refs.room_name.value, passRoom: this.refs.passRoom, user: this.state.user });
    }
    onSend(messages = []) {
        const { id } = this.state
        this.setState(previousState => ({
            messages: GiftedChat.append(previousState.messages, messages),
        }));
        this.sendnewMessage(messages)
    }
    render() {
        const { id } = this.state
        return (
            <GiftedChat
                messages={this.state.messages}
                onSend={messages => this.onSend(messages)}
                user={{
                    _id: id,
                }}
            />
        );
    }
}
