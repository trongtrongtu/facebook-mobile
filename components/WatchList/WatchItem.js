import React, { Component } from 'react'
import { View, Text, Image, StyleSheet, Dimensions, TouchableOpacity } from 'react-native'
import ScaledImage from '../ScaledImage'
import Icon from 'react-native-vector-icons/FontAwesome5'
import * as navigation from '../../rootNavigation'
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5'
import { permission } from '../../constants'
import VideoController from './VideoController'
import { connect } from 'react-redux'
import { SetFixedHeighWatchingVideo } from '../../actions/videoControlActions'
class WatchItem extends Component {
    constructor(props) {
        super(props)
        this._isLiked = { isLiked: false }
    }
    onPressHandle() {
        const { comments } = this.props.item
        navigation.navigate('CommentsPopUp', {
            comments
        })
    }
    onPressWatchOptionsIconHandler() {
        const { item } = this.props
        navigation.navigate('WatchOptions', {
            watchDetail: item
        })
    }
    onPressWatchVideoHandler(id, threadId) {
        navigation.navigate('WatchDetailList', {
            id,
            threadId
        })
    }
    onPressShareHandler() {
        const { item } = this.props
        navigation.navigate('SharePost', {
            id: item.id
        })
    }
    onLayoutHandler({ nativeEvent }) {
        const { setFixedHeighWatchingVideo } = this.props
        setFixedHeighWatchingVideo(nativeEvent.layout.height)
    }
    onPressLike(){
        if(this._isLiked.isLiked==false){
            this._isLiked.isLiked=true;
        }
        else{
            this._isLiked.isLiked=true;
        }
    }
    render() {
        const { item, user } = this.props
        let reactionValue = 0;
        for (let emoji in item.reactions) {
            reactionValue += item.reactions[emoji];
        }
        return (
            <View onLayout={this.onLayoutHandler.bind(this)} style={styles.item}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <View style={styles.customListView}>
                        <Image style={styles.avatar} source={{ uri: item.page.avatar_url }}></Image>
                        <View style={styles.infoWrapper}>
                            <View style={styles.namesWrapper}>
                                <TouchableOpacity>
                                    <Text style={{ fontSize: 16, fontWeight: '500' }}>{item.page.name}</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.extraInfoWrapper}>
                                <Text style={{ color: '#333', fontSize: 14 }}>{item.create_at}</Text>
                                <Text style={{ fontSize: 16, marginHorizontal: 5 }}>·</Text>
                                {item.permission == permission.PUBLIC && (
                                    <FontAwesome5Icon color='#333' name="globe-asia" />
                                )}
                                {item.permission == permission.SETTING && (
                                    // <FontAwesome5Icon color='#333' name="cogs" />
                                    <Image style={styles.icon2} source={require('../../assets/icons/settings.png')}></Image>
                                )}
                                {item.permission == permission.GROUP && (
                                    <FontAwesome5Icon color='#333' name="newspaper" />
                                )}
                            </View>
                        </View>
                    </View>
                    <TouchableOpacity onPress={this.onPressWatchOptionsIconHandler.bind(this)} style={{ width: 25, alignItems: 'center' }}>
                        <Icon name="ellipsis-h" color="#000"></Icon>
                    </TouchableOpacity>
                </View>
                <View style={styles.contentContainer}>
                    <Text style={styles.paragraph}>{item.content}</Text>
                </View>
                <TouchableOpacity activeOpacity={0.8} onPress={this.onPressWatchVideoHandler.bind(this, item.id, item.watch_threadId)}>
                    <View style={styles.videoContainer}>
                        <VideoController item={item} />
                    </View>
                </TouchableOpacity>

                <View style={styles.reactionValueWrapper}>
                    <TouchableOpacity >
                        <View style={styles.reactionNumberWrapper}>
                            {/* <FontAwesome5Icon name="thumbs-up" color="#318bfb" size={14}>
                                    </FontAwesome5Icon> */}
                            <Image style={styles.icon1} source={require('../../assets/icons/like5.png')}></Image>
                            <Text style={{ color: '#fff', marginLeft: 5 }}>{reactionValue}</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.onPressHandle.bind(this)}>
                        <Text style={{ color: '#807A7A' }}>{item.comments.length} bình luận</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.btnReactionWrapper}>
                    <TouchableOpacity style={styles.btnWrapper} onPress={this.onPressLike.bind(this)}>
                        <View style={styles.reactionBtn}>
                            {/* <FontAwesome5Icon name="thumbs-up" color={!this._isLiked.isLiked ? '#807A7A' : '#318bfb'} size={20}>
                                    </FontAwesome5Icon> */}
                            <Image style={styles.icon} source={!this._isLiked.isLiked ? require('../../assets/icons/like1.png') : require('../../assets/icons/like3.png')} />
                            <Text style={styles.reactionBtnText}>Thích</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btnWrapper} onPress={this.onPressHandle.bind(this)}>
                        <View style={styles.reactionBtn}>
                            <FontAwesome5Icon name="comment-alt" color="#807A7A" size={20}>
                            </FontAwesome5Icon>
                            <Text style={styles.reactionBtnText}>Bình luận</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btnWrapper} onPress={this.onPressShareHandler.bind(this)} >
                        <View style={styles.reactionBtn}>
                            {/* <FontAwesome5Icon name="share" color="#807A7A" size={20}>
                                    </FontAwesome5Icon> */}
                            <Image style={styles.icon} source={require('../../assets/icons/share.png')}></Image>
                            <Text style={styles.reactionBtnText}>Chia sẻ</Text>
                        </View>
                    </TouchableOpacity>
                </View>


            </View>
        )
    }
}
const mapStateToProps = state => {
    return {
        user: state.user.user
    }
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        setFixedHeighWatchingVideo: (height) => dispatch(SetFixedHeighWatchingVideo(height))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(WatchItem)
const screenWidth = Math.round(Dimensions.get('window').width);
const styles = StyleSheet.create({
    customListView: {
        padding: 15,
        width: screenWidth - 40,
        flexDirection: 'row'
    },
    icon2: {
        height: 12,
        resizeMode: 'contain',
        padding:0,

    },

    icon1: {
        height: 18,
        resizeMode: 'contain',
        padding:0,

    },

    icon: {
        height: 20,
        resizeMode: 'contain',
        padding:0,

    },
    avatar: {
        width: 40,
        height: 40,
        borderRadius: 50
    },
    infoWrapper: {
        marginLeft: 8
    },
    namesWrapper: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    extraInfoWrapper: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    item: {
        backgroundColor: '#fff',
        shadowColor: '#000',
        shadowOpacity: 0.3,
        shadowOffset: { height: 0, width: 0 },
        marginBottom: 10
    },
    commentInputWrapper: {
        width: "100%",
        height: "100%",
        justifyContent: 'center',
        borderRadius: 20,
        paddingHorizontal: 15
    },
    paragraph: {

    },
    contentContainer: {
        paddingHorizontal: 15
    },
    videoContainer: {
        width: screenWidth,
        marginTop: 5,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    reactionContainer: {
        position: 'relative',
        flexDirection: 'row',
        flexWrap: 'nowrap',
        alignItems: 'center'
    },
    reactionIcon: {
        fontSize: 20,
        padding: 10
    },
    shareIcon: {
        position: 'absolute',
        fontSize: 14,
        padding: 10,
        right: 0
    },
    commentContainer: {
        flexDirection: 'row',
        padding: 10,
        borderColor: "red",
        borderStyle: 'dashed',
        flexWrap: 'nowrap'
    },
    commentAvatar: {
        width: 30,
        height: 30,
        borderRadius: 50
    },
    commentInput: {
        borderWidth: 0.5,
        borderColor: 'gray',
        borderRadius: 20,
        marginLeft: 10,
        height: 30,
        width: screenWidth - 15 * 2 - 60,
    },
    btnSendComment: {
        width: 30,
        height: 30,
        textAlign: 'center',
        lineHeight: 30
    },btnReactionWrapper: {
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 10,
        paddingBottom:10,
        borderTopColor: '#ddd',
        borderTopWidth: 1
    },
    reactionBtnText: {
        color: '#807A7A',
        marginLeft: 5
    },
    btnWrapper: {
        flex:1
    },
    reactionBtn: {
        width: "100%",
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    reactionValueWrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 15,
        paddingHorizontal: 15
    },
    reactionNumberWrapper: {
        flexDirection: 'row',
        alignItems: 'center'
    }
})
