import React, { Component } from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Alert } from 'react-native';
import axios from 'axios'
import { AsyncStorage } from 'react-native';
const URL = 'https://it4895-nhom5.herokuapp.com/it4788'
export default class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            phone: '',
            password: ''
        }

    }
    login = () => {
        // this.getStorage();
        if (this.state.password != '' && this.state.phone != '') {

            axios.post(`${URL}/auth/login`, null, {
                params: {
                    phonenumber: this.state.phone,
                    password: this.state.password
                }
            }).then((response) => {
                this.setStorage('token', response && response.data && response.data.data && response.data.data.token)
                    .then(

                    )
                this.setStorage('user', JSON.stringify(response && response.data && response.data.data))
                    .then(
                        this.props.navigation.navigate('Home')
                    )

                // this.getStorage()
                //  Alert.alert('data',''+response);

                // console.log('aaaaa',response);
            }).catch(function (error) {
                console.log(error);
                Alert.alert('Bạn đã đăng nhập sai, hãy thử lại');
            });

        }


    }
    setStorage = async (key, value) => {
        try {
            await AsyncStorage.setItem(
                key,
                value
            );
            //	console.log(key+':',value);
        } catch (error) {
            // Error saving data
        }
    };
    getStorage = async (key) => {
        try {
            const value = await AsyncStorage.getItem(key);
            if (value !== null) {
                // We have data!!
                //   console.log(key+':', value);
                return value;
            }
        } catch (error) {
            // Error retrieving data
        }
    };

    render() {
        const { navigation } = this.props;
        return (
            <View style={styles.container}>
                <Text style={styles.logo}>facebook</Text>
                <View style={styles.inputView}>
                    <TextInput
                        style={styles.inputText}
                        placeholder="Số di động hoặc email"
                        placeholderTextColor="#003f5c"
                        onChangeText={text => this.setState({ phone: text })} />
                </View>
                <View style={styles.inputView}>
                    <TextInput
                        secureTextEntry
                        style={styles.inputText}
                        placeholder="Mật khẩu"
                        placeholderTextColor="#003f5c"
                        onChangeText={text => this.setState({ password: text })} />
                </View>
                <TouchableOpacity style={styles.loginBtn}
                    onPress={() => this.login()}>
                    <Text style={styles.loginText}>Đăng nhập</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => navigation.navigate('Find your account')}>
                    <Text style={styles.forgot}>Quên mật khẩu?</Text>
                </TouchableOpacity>
                <View style={styles.orText}>
                    <View style={styles.hairline} />
                    <Text style={styles.text}>HOẶC</Text>
                    <View style={styles.hairline} />
                </View>
                <TouchableOpacity style={styles.signupBtn}
                    onPress={() => navigation.navigate('Create Account')}>
                    <Text style={styles.signupText}>Tạo tài khoản Facebook mới</Text>
                </TouchableOpacity>
            </View>
        );
    }

}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    logo: {
        fontWeight: "bold",
        fontSize: 50,
        color: "#1877f2",
        marginBottom: 40
    },
    inputView: {
        width: "80%",
        backgroundColor: "#f5f6f7",
        borderRadius: 10,
        height: 50,
        marginBottom: 20,
        justifyContent: "center",
        padding: 20
    },
    inputText: {
        height: 50,
        color: "black"
    },
    loginText: {
        color: "#fff",
        fontWeight: "bold",
        fontSize: 17
    },
    forgot: {
        color: "#1877f2",
        fontSize: 16,
        fontWeight: "bold",
        marginBottom: 50
    },
    loginBtn: {
        width: "80%",
        backgroundColor: "#1877f2",
        borderRadius: 5,
        height: 40,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 40,
        marginBottom: 20
    },
    orText: {
        flexDirection: "row",
        alignItems: "center",
        paddingBottom: 100
    },
    hairline: {
        backgroundColor: '#A2A2A2',
        height: 1,
        width: 150,
        justifyContent: "center"
    },
    text: {
        fontSize: 12,
        paddingHorizontal: 5,
        color: "#080808"
    },
    signupBtn: {
        width: "60%",
        backgroundColor: "#00a400",
        borderRadius: 5,
        height: 35,
        alignItems: "center",
        justifyContent: "center",
        position: 'absolute',
        bottom: 0,
        marginBottom: 20
    },
    signupText: {
        color: "#fff",
        fontWeight: "bold"
    }
});
