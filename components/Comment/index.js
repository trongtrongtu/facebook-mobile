import React, { Component } from 'react'
import { View, Image, TouchableOpacity, Text, Dimensions, StyleSheet } from 'react-native'
import ScaledImage from '../ScaledImage'
import Icon from "react-native-vector-icons/FontAwesome5";
export default class Comment extends Component {
    convertTime(secs) {
        let t = new Date("1970"); // Epoch
        let now = new Date()
        t.setSeconds(secs);
        let t1 = t.getTime()
        let t2 = now.getTime()

        if(parseInt(t2-t1) <(3600 * 1000) ){
            let t33=(t2-t1)
            if(t33 > (60*1000)){
                return parseInt(t33 / (60*1000)) + ' phút';
            }else {
                return ' vừa đăng';
            }
        }else {
            let t3 = parseInt((t2 - t1) / (3600 * 1000)); // gio
            if (t3 < 24 ) {
                return parseInt(t3) + ' giờ';
            } else {
                let t3_ = parseInt(t3/(24)); //ngay
                if(t3_<7){
                    return parseInt(t3_) + ' ngày';
                }else {
                    return parseInt(t3_/7) + ' tuần ';
                }

            }
        }




    }
    render() {
        const { comment } = this.props;

        return (
            <View style={styles.container}>
                <Image style={styles.avatar} source={{ uri: (comment.poster.avatar && comment.poster.avatar.avatar) || 'https://qph.fs.quoracdn.net/main-qimg-2b21b9dd05c757fe30231fac65b504dd' }}></Image>
                <View style={styles.centerContainer}>
                    <View style={styles.contentContainer}>
                        <TouchableOpacity><Text style={styles.name}>{comment.poster.name}</Text></TouchableOpacity>
                        <Text style={styles.content}>{comment.comment}</Text>
                    </View>
                    {/* <ScaledImage width={screenWidth * 0.7} style={styles.image} source={comment.image}></ScaledImage> */}
                    <View style={styles.toolContainer}>
                        <Text style={styles.createAt}>{this.convertTime(comment.created)}</Text>
                        <TouchableOpacity style={styles.likeBtn}><Text >Thích</Text></TouchableOpacity>
                        <TouchableOpacity style={styles.replyBtn}><Text>Trả lời</Text></TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}
const screenWidth = Math.round(Dimensions.get('window').width);
const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        marginBottom: 15,

    },
    reactionIcon: {
        fontSize: 15,

    },
    avatar: {
        width: 40,
        height: 40,
        borderRadius: 50,
        marginRight: 10
    },
    centerContainer: {
        width: screenWidth * 0.7

    },
    contentContainer: {
        marginBottom: 10,
        padding: 10,
        paddingTop: 5,
        backgroundColor: '#e9ebee',
        borderRadius: 10,
    },
    name: {
        fontWeight: 'bold'
    },
    content: {

    },
    image: {
        borderRadius: 10
    },
    toolContainer: {
        marginTop: 2,
        flexDirection: 'row',
        width: 0.6 * screenWidth,

    },
    createAt: {
        flex: 1,
        marginLeft: 10
    },
    likeBtn: {
        textAlign: 'center',
        flex: 1
    },
    replyBtn: {
        textAlign: 'center',
        flex: 1
    }
})

