import React from 'react';
import {StyleSheet, Text, View, TextInput, TouchableOpacity} from "react-native";
import {RadioButton} from "react-native-paper";
import {color} from "react-native-reanimated";

export default function GenderRegister({route,navigation}) {
    const [checked, setChecked] = React.useState('first');
    const {ho, ten}=route.params;
    return (
        <View style={styles.container}>
            <Text style={styles.title}>Giới tính của bạn khi nào?</Text>
            <Text style={styles.text}>Về sau, bạn có thể thay đổ những ai nhìn thấy giới tính của mình trên trang cá
                nhân.</Text>
            <View style={styles.radioContainer}>
                <Text style={styles.radioText}>Nữ</Text>
                <RadioButton
                    style={styles.radioButton}
                    value="Female"
                    status={checked === 'first' ? 'checked' : 'unchecked'}
                    onPress={() => setChecked('first')}
                />
            </View>
            <View style={styles.radioContainer}>
                <Text style={styles.radioText}>Nam</Text>
                <RadioButton
                    value="Male"
                    status={checked === 'second' ? 'checked' : 'unchecked'}
                    onPress={() => setChecked('second')}
                />
            </View>
            <View style={styles.radioContainer}>
                <Text style={styles.radioText}>Tuỳ chỉnh</Text>
                <RadioButton
                    value="Option"
                    status={checked === 'third' ? 'checked' : 'unchecked'}
                    onPress={() => setChecked('third')}
                />
            </View>
            <TouchableOpacity style={styles.nextBtn}
                              onPress={() => navigation.navigate('Phone',{ho,ten})}>
                <Text style={styles.nextText}>Tiếp</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",
        alignItems: 'center'
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
        marginTop: 70,
        marginBottom: 10
    },
    text: {
        fontSize: 14,
        width: '90%',
        textAlign: 'center',
        color: "#837f7f",
        marginBottom: 20
    },
    radioContainer: {
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: "#a9a2a2",
        paddingBottom: 8,
        marginBottom: 10
    },
    radioText: {
        fontWeight: 'bold',
        fontSize: 16,
        width: "80%",
        textAlign: 'left',
        paddingTop: 5
    },
    nextBtn: {
        width: "90%",
        borderRadius: 5,
        height: 40,
        backgroundColor: "#1877f2",
        alignItems: "center",
        justifyContent: "center",
        marginTop: 80
    },
    nextText: {
        color: "#fff",
        fontSize: 15
    }
});
