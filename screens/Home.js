import React, { Component } from 'react';
import { connect } from 'react-redux'
import { FetchPostsRequest } from '../actions/postsAction'
import { LoginRequest, Login } from '../actions/userActions'
import { StyleSheet, Text, View, Button, ScrollView, Alert, RefreshControl } from 'react-native';
import { AsyncStorage } from 'react-native';
import { Dimensions } from "react-native";
import RecommendFriends from '../components/RecommendFriends'
import Item from '../components/Item'
import Stories from '../components/Stories'
import PostTool from '../components/PostTool'
import axios from 'axios'
const URL = 'https://it4895-nhom5.herokuapp.com/it4788'
class Home extends Component {
	state = {
		dataArr: [],
		token: '',
		refreshing: false
	}
	componentDidMount() {
		const { fetchPosts, postLogin } = this.props
		fetchPosts()
		postLogin()
		this.setState({ token: this.getStorage('token') });
		this.refreshDataFromServer();
		//	this.setState({dataArr: [...this.state.dataArr,this.getStorage('posts')] });
	}
	refreshDataFromServer = () => {
		// axios.post(`${URL}/auth/login?phonenumber=0365391481&password=trongtu`, {
		// 	//// phoneNumber: '0365391481',
		// 	// password: 'trongtu'
		// }).then((response) => {
		// 	this.setStorage(response && response.data && response.data.data && response.data.data.token)
		// 	// this.getStorage()
		this.setState({
			refreshing: true
		})
		axios.post(`${URL}/post/get_list_posts`, null,
			{
				params: {
					token: this.state.token,
					last_id: '0',
					index: '0',
					count: '20'
				}

			}).then((responsePost) => {
				console.log('aaaaa: ', responsePost && responsePost.data && responsePost.data.data && responsePost.data.data.posts)
				this.setState({
					dataArr: responsePost && responsePost.data && responsePost.data.data && responsePost.data.data.posts,
					refreshing: false
				})
			})
			.catch(function (error) {
				console.log(error);
				this.setState({
					refreshing: false
				})
			});
	}
	setStorage = async (key, value) => {
		try {
			await AsyncStorage.setItem(
				key,
				value
			);
		} catch (error) {
			// Error saving data
		}
	};
	getStorage = async (key) => {
		try {
			const value = await AsyncStorage.getItem(key);
			console.log('value: ', value)
			if (value !== null) {
				// We have data!!
				console.log(key + ':', value);
				return value;
			}
		} catch (error) {
			// Error retrieving data
		}
	};

	onRefresh = () => {
		this.setState({
			refreshing: true
		})
		console.log('state: ', this.state.token)
		console.log('componentDidMount', {
			token: this.state.token,
			last_id: '0',
			index: '0',
			count: '20'
		})
		axios.post(`${URL}/post/get_list_posts`, null,
			{
				params: {
					// token: this.state.token,
					last_id: '0',
					index: '0',
					count: '20'
				}

			}).then((responsePost) => {
				console.log('aaaaaaaa')
				// console.log('aaaaa: ', responsePost && responsePost.data && responsePost.data.data && responsePost.data.data.posts)
				this.setState({
					refreshing: false,
					dataArr: responsePost && responsePost.data && responsePost.data.data && responsePost.data.data.posts,
				})
			})
			.catch((error) => {
				this.setState({
					refreshing: false
				})
				console.log(error);
			});
	}

	render() {
		const { navigation } = this.props
		const { posts } = this.props
		const { dataArr, refreshing } = this.state
		if (dataArr.length === 0) return <View></View>
		return (
			<View>
				<ScrollView
					// bounces={false}
					style={styles.listContainter}
					refreshControl={
						<RefreshControl
							refreshing={refreshing}
							onRefresh={this.onRefresh}
						/>
					}
				>
					<PostTool  ></PostTool>
					<Stories></Stories>
					{dataArr.map((item, index) => (
						<View key={index}>
							{index === 1 && <RecommendFriends ></RecommendFriends>}
							<Item item={item} key={index} ></Item>
						</View>
					))}

				</ScrollView>
			</View >
		);
	}
}
const mapDispatchToProps = (dispatch, props) => {
	return {
		fetchPosts: () => dispatch(FetchPostsRequest()),
		postLogin: () => dispatch(LoginRequest("vucms", "vucms"))
	}
}
const mapStateToProps = (state) => {
	return {
		posts: state.posts
	}
}
export default connect(mapStateToProps, mapDispatchToProps)(Home)
const screenHeight = Math.round(Dimensions.get('window').height);
const styles = StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: 'column',
		backgroundColor: '#fff',
		alignItems: 'center',
		justifyContent: 'center',

	},
	countTxt: {
		fontSize: 200,
		textAlign: 'center',
		lineHeight: screenHeight - 170,
		width: "100%",
		height: screenHeight - 170
	},
	button: {
		width: "100%",
		height: 50,
		backgroundColor: 'black',
	},
	buttonText: {
		fontSize: 20,
		color: "white",
		textAlign: "center",
		lineHeight: 50
	}
});
