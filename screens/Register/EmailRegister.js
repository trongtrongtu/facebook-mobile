import React from 'react';
import {StyleSheet, Text, View, TextInput, TouchableOpacity} from "react-native";

export default function EmailRegister({navigation}) {
    return (
        <View style={styles.container}>
            <Text style={styles.title}>Nhập địa chỉ email của bạn</Text>
            <View style={styles.inputView}>
                <Text style={styles.emailText}>Địa chỉ email</Text>
                <TextInput
                    style={styles.inputText}/>
            </View>
            <TouchableOpacity style={styles.nextBtn}>
                <Text style={styles.nextText}>Tiếp</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.phoneBtn}
                              onPress={() => navigation.navigate('Phone')}>
                <Text style={styles.phoneText}>Đăng ký bằng số di động</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",
        alignItems: 'center'
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
        marginTop: 100,
        marginBottom: 30
    },
    emailText: {
        fontSize: 13,
        color: "#1877f2",
        alignItems: "flex-end"
    },
    inputView: {
        width: "90%",
        borderBottomWidth: 2,
        borderBottomColor: "#1877f2"

    },
    nextBtn: {
        width: "90%",
        borderRadius: 5,
        height: 40,
        backgroundColor: "#1877f2",
        alignItems: "center",
        justifyContent: "center",
        marginTop: 80
    },
    nextText: {
        color: "#fff",
        fontSize: 15
    },
    phoneBtn: {
        backgroundColor: "#fff",
        justifyContent: 'center',
        height: 50,
        position: 'absolute',
        bottom: 0
    },
    phoneText: {
        color: "#1877f2",
        fontWeight: 'bold'
    }
});