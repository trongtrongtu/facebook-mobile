import React, { Component } from 'react'
import { Text, StyleSheet, View, TouchableOpacity, Clipboard,Image } from 'react-native'
import Toast from 'react-native-root-toast';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5'
import * as navigation from '../../rootNavigation'
import ExTouchableOpacity from '../../components/ExTouchableOpacity';
export default class FriendOptions extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isVisible: false
        }
    }
    onPressCopyPostLinkHandler() {
        const { friend } = this.props.route.params
        setTimeout(() => {
            this.setState({
                ...this.state,
                isVisible: false
            })
        }, 2000)
        Clipboard.setString(`https://fakebook.com/posts/${friend.id}`)
        this.setState({
            ...this.state,
            isVisible: true
        })
    }
    onPressBackdropHandler() {
        navigation.goBack()
    }
    render() {
        const { friend } = this.props.route.params
        return (
            <View style={styles.container}>
                <View style={styles.backdrop}>
                    <ExTouchableOpacity onPress={this.onPressBackdropHandler.bind(this)} style={{ width: '100%', height: '100%' }}>

                    </ExTouchableOpacity>
                </View>
                <View style={styles.postOptionsWrapper}>
                    <TouchableOpacity style={styles.postOptionItemWrapper}>
                        <View style={styles.postOptionItem}>
                            <View style={styles.optionIcon}>
                                {/* <FontAwesome5Icon name="bookmark" size={24}></FontAwesome5Icon> */}
                                <Image style={styles.icon} source={require('../../assets/icons/friends.png')}></Image>
                                </View>
                            <View>
                                <Text style={styles.postOptionTitle}>Xem bạn bè của {friend.name}</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.postOptionItemWrapper}>
                        <View style={styles.postOptionItem}>
                            <View style={styles.optionIcon}>
                                {/* <FontAwesome5Icon name="minus-square" size={24}></FontAwesome5Icon> */}
                                <Image style={styles.icon} source={require('../../assets/icons/messenger.png')}></Image>
                                </View>
                            <View>
                                <Text style={styles.postOptionTitle}>Nhắn tin cho {friend.name}</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.postOptionItemWrapper}>
                        <View style={styles.postOptionItem}>
                            <View style={styles.optionIcon}>
                                {/* <FontAwesome5Icon name="globe-asia" size={24}></FontAwesome5Icon> */}
                                <Image style={styles.icon} source={require('../../assets/icons/remove.png')}></Image>
                                </View>
                            <View>
                                <Text style={styles.postOptionTitle}>Bỏ theo dõi {friend.name}</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.postOptionItemWrapper}>
                        <View style={styles.postOptionItem}>
                            <View style={styles.optionIcon}>
                                {/* <FontAwesome5Icon name="trash-alt" size={24}></FontAwesome5Icon> */}
                                <Image style={styles.icon} source={require('../../assets/icons/block-user.png')}></Image>
                                </View>
                            <View>
                                <Text style={styles.postOptionTitle}>Chặn {friend.name}</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.postOptionItemWrapper}>
                        <View style={styles.postOptionItem}>
                            <View style={styles.optionIcon}>
                                {/* <FontAwesome5Icon name="history" size={24}></FontAwesome5Icon> */}
                                <Image style={styles.icon} source={require('../../assets/icons/remove-contact.png')}></Image>
                                </View>
                            <View>
                                <Text style={styles.postOptionTitle}>Hủy kết bạn với {friend.name} </Text>
                            </View>
                        </View>
                    </TouchableOpacity>

                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: "100%",
        width: '100%',
        position: 'relative',

    },
    backdrop: {
        height: '100%',
        width: '100%',
        zIndex: 1
    },
    postOptionsWrapper: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        width: '100%',
        zIndex: 2,
        padding: 15,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 15,
    },
    postOptionItemWrapper: {
        paddingBottom: 20
    },
    postOptionItem: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    optionIcon: {
        width: 35,
        alignItems: 'center'
    },
    postOptionTitle: {
        fontSize: 16
    },
    postOptionSubtitle: {
        fontSize: 12
    }
})
