import React, { useState, useEffect } from 'react';
import {StyleSheet, Text, View, TextInput, TouchableOpacity, Alert} from "react-native";

export default function PhoneRegister({route,navigation}) {
    const {ho, ten}=route.params;
    const [phone, setPhone]=useState('');
    const checkPhone=()=>{
        let vnf_regex = /((09|03|07|08|05)+([0-9]{8})\b)/g;
        if(phone !==''){
            if (vnf_regex.test(phone) == false)
            {
                Alert.alert('Thông báo','Số điện thoại của bạn không đúng định dạng');
            }else{
               // Alert.alert('Thông báo','Số điện thoại của bạn hợp lệ');
                navigation.navigate('Password',{ho,ten,phone});
            }
        }else{
            Alert.alert('Thông báo','Bạn chưa điền số điện thoại');
        }

    }
    return (
        <View style={styles.container}>
            <Text style={styles.title}>Nhập số di động của bạn</Text>
            <View style={styles.inputView}>
                <Text style={styles.phoneText}>Số di động</Text>
                <TextInput
                    style={styles.inputText}
                    onChangeText={text => setPhone(text)}
                />
            </View>
            <TouchableOpacity style={styles.nextBtn} onPress={() => checkPhone()}>
                <Text style={styles.nextText}>Tiếp</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.mailBtn}
                              onPress={() => navigation.navigate('Email')}>
                <Text style={styles.mailText}>Đăng ký bằng địa chỉ email</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",
        alignItems: 'center'
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
        marginTop: 100,
        marginBottom: 30
    },
    phoneText: {
        fontSize: 13,
        color: "#1877f2",
        alignItems: "flex-end"
    },
    inputView: {
        width: "90%",
        borderBottomWidth: 2,
        borderBottomColor: "#1877f2"

    },
    nextBtn: {
        width: "90%",
        borderRadius: 5,
        height: 40,
        backgroundColor: "#1877f2",
        alignItems: "center",
        justifyContent: "center",
        marginTop: 80
    },
    nextText: {
        color: "#fff",
        fontSize: 15
    },
    mailBtn: {
        backgroundColor: "#fff",
        justifyContent: 'center',
        height: 50,
        position: 'absolute',
        bottom: 0
    },
    mailText: {
        color: "#1877f2",
        fontWeight: 'bold'
    }
});
