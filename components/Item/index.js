import React, { Component } from 'react'
import {
    View,
    Text,
    Image,
    StyleSheet,
    Dimensions,
    TouchableOpacity,
    Alert,
    AsyncStorage,
    RefreshControl,
    Animated,
} from 'react-native'
import ScaledImage from '../ScaledImage'
import Icon from 'react-native-vector-icons/FontAwesome5'
import * as navigation from '../../rootNavigation'
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5'
import { permission } from '../../constants'
import { connect } from 'react-redux'
import axios from "axios";
import { Video } from "expo-av";
import { SCREEN_WIDTH } from '../../constants';
import ExTouchableOpacity from "../ExTouchableOpacity";
import VideoController from "../WatchList/VideoController";
const URL = 'https://it4895-nhom5.herokuapp.com/it4788'
class Item extends Component {
    constructor(props) {
        super(props)
        this.state = {
            token: '',
            comments: [],
            // xu li like
            countLike: 0,
            like: '',

            // control video
            mute: true,
            shouldPlay: true
        }
    }

    componentDidMount() {
        let item = this.props.item
        this.setState({ like: item.is_liked, countLike: parseInt(item.like) });
        this.getStorage('token')
            .then(value => {
                this.setState({ token: value });
                this.refreshDataFromServer();
            })

    }
    getStorage = async (key) => {
        try {
            const value = await AsyncStorage.getItem(key);
            if (value !== null) {
                // We have data!!
                return value;
            }
        } catch (error) {
            // Error retrieving data
        }
    };
    refreshDataFromServer = () => {
        const item = this.props.item;
        axios.post(`${URL}/comment/get_comment`, null,
            {
                params: {
                    token: this.state.token,
                    id: item.id,
                    index: 0,
                    count: 20
                }
            }).then((response) => {
                this.setState({
                    comments: response.data && response.data.data
                });

            }).catch(err => {
                console.log('err', err + ' token ' + this.state.token)
            })
    }
    onPressHandle() {
        const item = this.props.item;
        const { comments } = this.state;
        navigation.navigate('Comments', {
            comments, item
        });
    }
    onPressPostOptionsIconHandler() {
        const { item } = this.props
        navigation.navigate('PostOptions', {
            postDetail: item
        })
    }
    onPressPostImageHandler(id) {
        // navigation.navigate('PostDetail', {
        //     id
        // })
    }
    onPressShareHandler() {
        const { item } = this.props
        navigation.navigate('SharePost', {
            id: item.id
        })
    }
    onPressProfileHandler(userId) {
        const { user } = this.props
        if (userId === user.id) {
            return navigation.navigate('Profile')
        }
        navigation.push('ProfileX', {
            userId
        })
    }
    convertTime(secs) {
        var t = new Date("1970"); // Epoch

        t.setSeconds(secs);
        var time = t.getDate() + ' thang ' + t.getMonth() + ' luc ' + t.getHours() + ':' + t.getMinutes();
        return time;
    }
    onPressLike() {
        //   this.setState({ like: !this.state.like })
        if (this.state.like == true) {
            this.setState({ like: false, countLike: this.state.countLike - 1 });
        } else {
            this.setState({ like: true, countLike: this.state.countLike + 1 });
        }
        this.refreshLikeFromServer();
    }
    refreshLikeFromServer() {
        let post_id = this.props.item.id;
        // Alert.alert('post id',this.state.token);
        axios.post(`${URL}/like/like`, null,
            {
                params: {
                    token: this.state.token,
                    id: post_id,
                }
            }).then((response) => {
                // console.log('response comments : ', response.data && response.data.data)
                // this.setState({
                //     comments: response.data && response.data.data
                // })
            })
    }
    handlePlayAndPause() {
        this.setState({ shouldPlay: !this.state.shouldPlay })
    }
    handleVolume() {
        this.setState({ mute: !this.state.mute })
    }
    render() {
        const { user, item } = this.props
        let reactionValue = 0;
        for (let emoji in item.reactions) {
            reactionValue += item.reactions[emoji];
        }
        return (

            <View style={styles.item} >
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <View style={styles.customListView}>
                        <Image style={styles.avatar} source={{ uri: (item.author && item.author.avatar && item.author.avatar.avatar) || 'https://qph.fs.quoracdn.net/main-qimg-2b21b9dd05c757fe30231fac65b504dd' }}></Image>
                        <View style={styles.infoWrapper}>
                            <View style={styles.namesWrapper}>
                                <TouchableOpacity onPress={this.onPressProfileHandler.bind(this, item.author && item.author.id)}>
                                    <Text style={{ fontSize: 16, fontWeight: '500' }}>{item.author && item.author.username}</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.extraInfoWrapper}>
                                <Text style={{ color: '#333', fontSize: 14 }}>{this.convertTime(parseInt(item.created))}</Text>
                                <Text style={{ fontSize: 16, marginHorizontal: 5 }}>·</Text>
                                {item.permission == permission.PUBLIC && (
                                    <FontAwesome5Icon color='#333' name="globe-asia" />
                                )}
                                {item.permission == permission.SETTING && (
                                    // <FontAwesome5Icon color='#333' name="cogs" />
                                    <Image style={styles.icon2} source={require('../../assets/icons/settings.png')}></Image>
                                )}
                                {item.permission == permission.GROUP && (
                                    <FontAwesome5Icon color='#333' name="newspaper" />
                                )}
                            </View>
                        </View>
                    </View>
                    <TouchableOpacity onPress={this.onPressPostOptionsIconHandler.bind(this)} style={{ width: 25, alignItems: 'center' }}>
                        <Icon name="ellipsis-h" color="#000"></Icon>
                    </TouchableOpacity>
                </View>
                <View style={styles.contentContainer}>
                    <Text style={styles.paragraph}>{item.described}</Text>
                </View>
                <TouchableOpacity onPress={this.onPressPostImageHandler.bind(this, item.id)}>
                    {item.image != null ? (

                        <>

                            {item.image.length === 1 ? (

                                <View style={styles.imageContainer}>
                                    <ScaledImage source={item.image[0].url || ''}></ScaledImage>
                                </View>

                            ) : (
                                    <>

                                        {item.image.length > 1 ?
                                            (
                                                <View style={{ flexWrap: 'wrap', flex: 1, flexDirection: 'row' }}>

                                                    {item.image.map((img, index) =>
                                                        (
                                                            <View key={index}>
                                                                <ScaledImage style={{ width: 120, height: 120, flex: 1, marginLeft: 5, marginRight: 5 }} source={img.url || ''}></ScaledImage>
                                                            </View>
                                                        )

                                                    )}

                                                </View>
                                            ) : (

                                                <ExTouchableOpacity /*onPress={this.onPressWatchVideoDetail.bind(this)}*/ >
                                                    <View style={styles.videoWrapper}>
                                                        <Video
                                                            posterStyle={styles.posterStyle}
                                                            usePoster={true}
                                                            shouldPlay={this.state.shouldPlay}
                                                            isMuted={this.state.mute}
                                                            resizeMode="cover"
                                                            source={{ uri: item.video.url }}
                                                            style={styles.video}>
                                                        </Video>

                                                        <TouchableOpacity style={styles.btnToggleVolume} >



                                                            {this.state.shouldPlay ? (
                                                                <Animated.View style={{ paddingLeft: 50 }} >
                                                                    <FontAwesome5Icon color='#fff' name="pause-circle" size={20} onPress={() => this.handlePlayAndPause()} />
                                                                </Animated.View>
                                                            ) : (
                                                                    <Animated.View style={{ paddingLeft: 50 }}>
                                                                        <FontAwesome5Icon color='#fff' name="play-circle" size={20} onPress={() => this.handlePlayAndPause()} />
                                                                    </Animated.View>
                                                                )}


                                                            {this.state.mute ? (
                                                                <Animated.View style={{ position: 'absolute' }}>
                                                                    <FontAwesome5Icon color='#fff' name="volume-mute" size={20} onPress={() => this.handleVolume()} />
                                                                </Animated.View>
                                                            ) : (
                                                                    <Animated.View style={{ position: 'absolute' }}>
                                                                        <FontAwesome5Icon color='#fff' name="volume-up" size={20} onPress={() => this.handleVolume()} />
                                                                    </Animated.View>
                                                                )}

                                                        </TouchableOpacity>






                                                    </View>
                                                    <View style={styles.controlBar} >

                                                    </View>
                                                </ExTouchableOpacity>
                                            )

                                            // <VideoController item={item} />


                                        }
                                    </>
                                )}
                        </>
                    ) : (
                            <>

                            </>
                        )}
                </TouchableOpacity>
                {/* <View horizontal={true} style={styles.reactionContainer}>
                    <TouchableOpacity><Icon
                        name="thumbs-up"
                        color="#318bfb"
                        backgroundColor="#fff"
                        style={styles.reactionIcon}
                    >
                        <Text style={{ fontSize: 12 }}> {item.like} likes</Text>
                    </Icon></TouchableOpacity>

                    <TouchableOpacity onPress={this.onPressHandle.bind(this)}><Icon
                        lineBreakMode={false}
                        name="comment-alt"
                        color="gray"
                        backgroundColor="white"
                        style={{ ...styles.reactionIcon, fontSize: 14 }}
                    ><Text style={{ fontSize: 12 }}> {item.comment} comments</Text></Icon></TouchableOpacity>
                    <TouchableOpacity onPress={this.onPressShareHandler.bind(this)} style={styles.shareIcon}><Icon name="share-alt"
                        color="gray" ><Text style={{ fontSize: 12, textAlignVertical: 'center' }}> Share</Text></Icon></TouchableOpacity>
                </View> */}
                <View style={styles.reactionValueWrapper}>
                    <TouchableOpacity >
                        <View style={styles.reactionNumberWrapper}>
                            {/* <FontAwesome5Icon name="thumbs-up" color="#318bfb" size={14}>
                                    </FontAwesome5Icon> */}
                            <Image style={styles.icon1} source={require('../../assets/icons/like5.png')}></Image>
                            {/*<Text style={{marginLeft: 5 }}>{this.state.countLike}</Text>*/}
                            <Text style={{ marginLeft: 5 }}>
                                {
                                    this.state.like == true ?
                                        (
                                            <Text>Bạn {(this.state.countLike - 1) > 0 ? ('và ' + (this.state.countLike - 1) + ' người khác') : ''}</Text>
                                        )
                                        :
                                        (
                                            <Text>{this.state.countLike}</Text>
                                        )
                                }
                            </Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.onPressHandle.bind(this)}>
                        <Text style={{ color: '#807A7A' }}>{item.comment && item.comment} bình luận</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.btnReactionWrapper}>
                    <TouchableOpacity style={styles.btnWrapper} onPress={this.onPressLike.bind(this)}>
                        <View style={styles.reactionBtn}>
                            {/* <FontAwesome5Icon name="thumbs-up" color={!this._isLiked.isLiked ? '#807A7A' : '#318bfb'} size={20}>
                                    </FontAwesome5Icon> */}
                            <Image style={styles.icon} source={this.state.like == true ? require('../../assets/icons/like1.png') : require('../../assets/icons/like3.png')} />
                            <Text style={{ color: this.state.like == true ? '#2078f4' : '#807A7A' }}>Thích</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btnWrapper} onPress={this.onPressHandle.bind(this)}>
                        <View style={styles.reactionBtn}>
                            <FontAwesome5Icon name="comment-alt" color="#807A7A" size={20}>
                            </FontAwesome5Icon>
                            <Text style={styles.reactionBtnText}>Bình luận</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btnWrapper} onPress={this.onPressShareHandler.bind(this)} >
                        <View style={styles.reactionBtn}>
                            {/* <FontAwesome5Icon name="share" color="#807A7A" size={20}>
                                    </FontAwesome5Icon> */}
                            <Image style={styles.icon} source={require('../../assets/icons/share.png')}></Image>
                            <Text style={styles.reactionBtnText}>Chia sẻ</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                {/* <View style={styles.commentContainer}>
                    <Image source={{ uri: (item.author && item.author.avatar && item.author.avatar.avatar) || 'https://qph.fs.quoracdn.net/main-qimg-2b21b9dd05c757fe30231fac65b504dd' }} style={styles.commentAvatar}></Image>
                    <View style={styles.commentInput}>
                        <TouchableOpacity onPress={()=> this.onPressHandle()} style={styles.commentInputWrapper}>
                            <Text>Comment...</Text>
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity><Icon style={styles.btnSendComment} name="paper-plane" color="gray"></Icon></TouchableOpacity>
                </View> */}
            </View>
        )
    }
}
const mapStateToProps = state => {
    return {
        user: state.user.user
    }
}
export default connect(mapStateToProps, null)(Item)
const screenWidth = Math.round(Dimensions.get('window').width);
const styles = StyleSheet.create({
    customListView: {
        padding: 15,
        width: screenWidth - 40,
        flexDirection: 'row'
    },
    icon2: {
        height: 12,
        resizeMode: 'contain',
        padding: 0,

    },

    icon1: {
        height: 18,
        resizeMode: 'contain',
        padding: 0,

    },

    icon: {
        height: 20,
        resizeMode: 'contain',
        padding: 0,

    },
    avatar: {
        width: 40,
        height: 40,
        borderRadius: 50
    },
    infoWrapper: {
        marginLeft: 8
    },
    namesWrapper: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    extraInfoWrapper: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    item: {
        backgroundColor: '#fff',
        shadowColor: '#000',
        shadowOpacity: 0.3,
        shadowOffset: { height: 0, width: 0 },
        marginBottom: 10
    },
    btnReactionWrapper: {
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 10,
        paddingBottom: 10,
        borderTopColor: '#ddd',
        borderTopWidth: 1
    },
    reactionBtnText: {
        color: '#807A7A',
        marginLeft: 5
    },
    btnWrapper: {
        flex: 1
    },
    reactionBtn: {
        width: "100%",
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    commentInputWrapper: {
        width: "100%",
        height: "100%",
        justifyContent: 'center',
        borderRadius: 20,
        paddingHorizontal: 15
    },
    paragraph: {

    },
    contentContainer: {
        paddingHorizontal: 15
    },
    imageContainer: {
        marginTop: 5,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    reactionContainer: {
        position: 'relative',
        flexDirection: 'row',
        flexWrap: 'nowrap',
        alignItems: 'center'
    },
    reactionIcon: {
        fontSize: 20,
        padding: 10
    },
    shareIcon: {
        position: 'absolute',
        fontSize: 14,
        padding: 10,
        right: 0
    },
    commentContainer: {
        flexDirection: 'row',
        padding: 10,
        borderColor: "red",
        borderStyle: 'dashed',
        flexWrap: 'nowrap'
    },
    commentAvatar: {
        width: 30,
        height: 30,
        borderRadius: 50
    },
    commentInput: {
        borderWidth: 0.5,
        borderColor: 'gray',
        borderRadius: 20,
        marginLeft: 10,
        height: 30,
        width: screenWidth - 15 * 2 - 60,
    },
    btnSendComment: {
        width: 30,
        height: 30,
        textAlign: 'center',
        lineHeight: 30
    },
    reactionValueWrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 15,
        paddingHorizontal: 15
    },
    reactionNumberWrapper: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    video: {
        width: SCREEN_WIDTH,
        height: 300,
        backgroundColor: '#000'
    },
    controlBar: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        width: (SCREEN_WIDTH - 15 * 2 - 20),
        right: 0,
        height: 20,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "rgba(0,0,0,0.5)"
    },
    btnToggleVolume: {
        position: 'absolute',
        bottom: 20,
        right: 20
    },

})
