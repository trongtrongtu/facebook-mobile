
## Installation
```bash
yarn
yarn add json-server -g
```

## Usage
Config the variable BASE_URL in ./constants/index to your private ip example 192.168.1.x:3000
```bash
yarn dbstart
yarn start 
```
