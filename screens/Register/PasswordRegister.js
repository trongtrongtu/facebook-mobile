import React, {useState, useEffect, Component} from 'react';
import {StyleSheet, Text, View, TextInput, TouchableOpacity, Alert, AsyncStorage} from "react-native";
import axios from "axios";
const URL = 'https://it4895-nhom5.herokuapp.com/it4788';
export default class PasswordRegister extends Component  {
    constructor(props) {
        super(props)
        this.state = {
            phone: '',
            password:'',
            confPassword:'',
            id:'',
            verifyCode:'',
            token:'',
            ho:'',
            ten:''
        }


    }
    componentDidMount() {
        const {ho,ten,phone}=this.props.route.params;
        this.setState({
            ho:ho,
            ten:ten,
            phone:phone
        })
    }

    checkPass=()=>{
        let regex =/^.{6,}$/;
        if(this.state.password == this.state.confPassword){
            if (regex.test(this.state.password) == false){
                Alert.alert('Thong bao','Hãy nhập mật khẩu trên 6 kí tự')
            }else {
               // Alert.alert('Thong bao',phone)
                this.register();
            }
        }else{
            Alert.alert('Thong bao','Nhập sai password')
        }
    }
    register=()=>{
        const {ho,ten,phone}=this.props.route.params;
          axios.post(`${URL}/auth/signup`, null,
            {
                params: {
                    phonenumber: phone,
                    password: this.state.password,
                }
            }).then((response) => {
                this.setState({id:response.data.data.id});
              this.setState({verifyCode:response.data.data.verifyCode});

               // console.log('verifyCode', user_id)
                this.checkVerifyCode();
        }).catch(err =>{
            console.log('err',err)
              Alert.alert('Xay ra loi, hãy thử lại ');
        })
    }

     checkVerifyCode =()=>{
         const {ho,ten,phone}=this.props.route.params;
        axios.post(`${URL}/auth/check_verify_code`, null,
            {
                params: {
                    phonenumber: phone,
                    code_verify: this.state.verifyCode,
                }
            }).then((response) => {
            this.setState({token:response.data && response.data.data && response.data.data.token });
            this.setUser();

        }).catch(err =>{
            console.log('err',err)
            Alert.alert('Xay ra loi, hãy thử lại ');
        })
    }
     login=()=>{
         const {ho,ten,phone}=this.props.route.params;
        axios.post(`${URL}/auth/login`, null,{
            params:{
                phonenumber: phone,
                password: this.state.password
            }
        }).then((response) => {
            this.setStorage('token',response && response.data && response.data.data && response.data.data.token)
                .then(
                )
            this.setStorage('user',JSON.stringify(response && response.data && response.data.data))
                .then(
                    this.props.navigation.navigate('Home')
                )
        }).catch(function (error) {
            console.log(error);
            Alert.alert('Xay ra loi, hãy thử lại login');
        });
    }
     setUser=()=>{
         const {ho,ten,phone}=this.props.route.params;
        axios.post(`${URL}/user/set_user_info?`, null,{
            params:{
                token: this.state.token,
                username: ho+ten,
                description:'phone : '+phone
            }
        }).then((response) => {
            this.login();
        }).catch(function (error) {
            console.log(error);
            Alert.alert('Xay ra loi, hãy thử lại setUser');
        });
    }
    setStorage = async (key,value) => {
        try {
            await AsyncStorage.setItem(
                key,
                value
            );
            //	console.log(key+':',value);
        } catch (error) {
            // Error saving data
        }
    };
    render() {



    return (
        <View style={styles.container}>
            <Text style={styles.title}>Chọn mật khẩu</Text>

            <View style={styles.inputView}>
                <Text style={styles.passText}>Mật khẩu</Text>
                <TextInput
                    style={styles.textInput}
                    secureTextEntry={true}
                    onChangeText={text => this.setState({password: text})}
                />
            </View>
            <View style={styles.inputView}>
                <Text style={styles.passText}>Nhập lại mật khẩu</Text>
                <TextInput
                    style={styles.textInput}
                    secureTextEntry={true}
                    onChangeText={text => this.setState({confPassword: text})}
                />
            </View>
            <TouchableOpacity style={styles.nextBtn} onPress={()=>this.checkPass()}>
                <Text style={styles.nextText}>Tiếp</Text>
            </TouchableOpacity>
        </View>
    );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",
        alignItems: 'center'
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
        marginTop: 90,
        marginBottom: 30
    },
    passText: {
        fontSize: 13,
        color: "#1877f2",
        alignItems: "flex-end"
    },
    inputView: {
        width: "90%",
        borderBottomWidth: 2,
        borderBottomColor: "#1877f2",
        marginTop:30
    },
    nextBtn: {
        width: "90%",
        borderRadius: 5,
        height: 40,
        backgroundColor: "#1877f2",
        alignItems: "center",
        justifyContent: "center",
        marginTop: 80
    },
    nextText: {
        color: "#fff",
        fontSize: 15
    }
});
