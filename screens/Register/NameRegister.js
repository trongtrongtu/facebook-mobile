import React, { useState } from 'react';
import {StyleSheet, Text, View, TextInput, TouchableOpacity, Alert} from "react-native";

export default function NameRegister({navigation}) {
    const [ten, setTen] = useState('');
    const [ho, setHo] = useState('');
    const checkInput = ()=>{
        if(ho !='' && ten !=''){
            navigation.navigate('DoB',{ten,ho})
        }else {
            Alert.alert('Thông báo','Không được bỏ trống');
        }

    }
    return (
        <View style={styles.container}>
            <Text style={styles.title}>Bạn tên gì?</Text>
            <View style={styles.inputNameView}>
                <View style={styles.inputView}>
                    <TextInput
                        style={styles.inputText}
                        placeholder="Họ"
                        onChangeText={text => setHo(text)}
                    />

                </View>
                <View style={styles.inputView}>
                    <TextInput
                        style={styles.inputText}
                        placeholder="Tên"
                        onChangeText={text => setTen(text)}
                    />
                </View>
            </View>
            <TouchableOpacity style={styles.nextBtn}
                              onPress={() => checkInput()}>
                <Text style={styles.nextText}>Tiếp</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",
        alignItems: 'center'
    },
    title: {
        fontSize: 18,
        fontWeight: 'bold',
        marginTop: 100,
        marginBottom: 30
    },
    inputNameView: {
        flexDirection: 'row'
    },
    inputView: {
        width: "45%",
        borderBottomWidth: 2,
        borderBottomColor: "#1877f2",
        height: 40,
        marginEnd: 5,
        marginStart: 5
    },
    inputText: {
        fontSize: 16
    },
    nextBtn: {
        width: "90%",
        borderRadius: 5,
        height: 45,
        backgroundColor: "#1877f2",
        alignItems: "center",
        justifyContent: "center",
        marginTop: 110
    },
    nextText: {
        color: "#fff",
        fontSize: 15
    }
})
