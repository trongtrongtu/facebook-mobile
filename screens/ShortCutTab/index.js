import React, { Component } from 'react'
import { View, Text, ScrollView, Image, StyleSheet, TouchableOpacity, Animated } from 'react-native'
import { connect } from 'react-redux'
import ExTouchableOpacity from '../../components/ExTouchableOpacity'
import { navigation } from '../../rootNavigation'
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5'
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome'
import { SCREEN_WIDTH } from '../../constants'
import { AsyncStorage } from "react-native";
class index extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isShowMore: false
        }
        this._maxOptionsHeight = 9 * 50
        this._moreOptionsHeight = new Animated.Value(0)
    }
    onPressViewMyProfileHandler() {
        navigation.navigate('Profile')
    }
    onPressFullFriendsHandler() {
        const { friends } = this.props
        navigation.navigate('FullFriends', { friends })
    }
    onPressWatchVideoHandler() {
        navigation.navigate('Watch')
    }
    onPressGroupsHandler() {
        navigation.navigate('Group')
    }
    onpressFriendAroundHandler() {
        navigation.navigate('FindFriends')
    }
    onPressMarketplaceHandler() {
        navigation.navigate('Marketplace')
    }
    onPressToggleShowHandler() {
        if (this.state.isShowMore) {
            Animated.timing(this._moreOptionsHeight, {
                toValue: 0,
                duration: 400
            }).start()
        } else Animated.timing(this._moreOptionsHeight, {
            toValue: this._maxOptionsHeight,
            duration: 600
        }).start()
        this.setState({
            ...this.state,
            isShowMore: !this.state.isShowMore
        })
    }
    logout = () => {

            this.removeStorage().then(
                this.props.navigation.navigate('Login')
            )



    }
    removeStorage = async () => {
        try {
            await AsyncStorage.removeItem('token');
            await AsyncStorage.clear();
        }
        catch (exception) {
            console.log(exception)
        }
    }
    render() {
        const { user } = this.props
        const { isShowMore } = this.state
        return (
            <View style={styles.container}>
                <ScrollView bounces={false}>
                    <ExTouchableOpacity style={styles.btnProfile} onPress={this.onPressViewMyProfileHandler}>
                        <Image style={styles.avatar} source={{ uri: user.avatar_url }} />
                        <View>
                            <Text style={styles.name}>{user.name}</Text>
                            <Text style={{ color: '#333' }}>Xem trang cá nhân của bạn</Text>
                        </View>
                    </ExTouchableOpacity>
                    <ExTouchableOpacity onPress={this.onPressWatchVideoHandler} style={styles.btnOption} >
                        <Image style={styles.icon} source={require('../../assets/icons/video.png')} />
                        <View>
                            <Text style={styles.name}>Video trên Watch</Text>
                            <Text style={{ color: '#333' }}>X+ video mới</Text>
                        </View>
                    </ExTouchableOpacity>
                    <ExTouchableOpacity style={styles.btnOption} >
                        <Image style={styles.icon} source={require('../../assets/icons/bookmark.png')} />
                        <View>
                            <Text style={styles.name}>Đã lưu</Text>
                        </View>
                    </ExTouchableOpacity>
                    <ExTouchableOpacity style={styles.btnOption} >
                        <Image style={styles.icon} source={require('../../assets/icons/live-news.png')} />
                        <View>
                            <Text style={styles.name}>Video trực tiếp</Text>
                        </View>
                    </ExTouchableOpacity>
                    <ExTouchableOpacity onPress={this.onPressFullFriendsHandler.bind(this)} style={styles.btnOption} >
                        {/* <Image style={styles.icon} source={require('../../assets/icons/friendship.png')} /> */}
                        <FontAwesome5Icon name={"user-friends"} size={20} color="#0B66C4" />
                        <View>
                            <Text style={styles.name}>    Bạn bè</Text>
                        </View>
                    </ExTouchableOpacity>
                    <ExTouchableOpacity onPress={this.onPressGroupsHandler} style={styles.btnOption} >
                        <Image style={styles.icon} source={require('../../assets/icons/multiple-users-silhouette.png')} />
                        <View>
                            <Text style={styles.name}>Nhóm</Text>
                        </View>
                    </ExTouchableOpacity>
                    <ExTouchableOpacity onPress={this.onPressMarketplaceHandler} style={styles.btnOption} >
                        <Image style={styles.icon} source={require('../../assets/icons/marketplace.png')} />
                        <View>
                            <Text style={styles.name}>Marketplace</Text>
                        </View>
                    </ExTouchableOpacity>
                    <ExTouchableOpacity style={styles.btnOption} >
                        <Image style={styles.icon} source={require('../../assets/icons/calendar.png')} />
                        <View>
                            <Text style={styles.name}>Sự kiện</Text>
                        </View>
                    </ExTouchableOpacity>
                    <ExTouchableOpacity style={styles.btnOption} >
                        <Image style={styles.icon} source={require('../../assets/icons/history1.png')} />
                        <View>
                            <Text style={styles.name}>Kỷ niệm</Text>
                        </View>
                    </ExTouchableOpacity>
                    <ExTouchableOpacity style={styles.btnOption} >
                        <Image style={styles.icon} source={require('../../assets/icons/flag.png')} />
                        <View>
                            <Text style={styles.name}>Trang</Text>
                            <Text style={{ color: '#333' }}>X+ trang mới</Text>
                        </View>
                    </ExTouchableOpacity>
                    <ExTouchableOpacity onPress={this.onpressFriendAroundHandler} style={styles.btnOption} >
                        {/* <Image style={styles.icon} source={require('../../assets/icons/planet.png')} /> */}
                        <FontAwesomeIcon name={"user-circle-o"} size={20} color="#0B66C4" />
                        <View>
                            <Text style={styles.name}>     Bạn bè quanh đây</Text>
                        </View>
                    </ExTouchableOpacity>
                    <ExTouchableOpacity style={styles.btnOption} >
                        <Image style={styles.icon} source={require('../../assets/icons/controller.png')} />
                        <View>
                            <Text style={styles.name}>Chơi game</Text>
                        </View>
                    </ExTouchableOpacity>
                    <ExTouchableOpacity style={styles.btnOption} >
                        <Image style={styles.icon} source={require('../../assets/icons/portfolio.png')} />
                        <View>
                            <Text style={styles.name}>Việc làm</Text>
                        </View>
                    </ExTouchableOpacity>
                    <TouchableOpacity style={styles.btnOption} onPress={this.onPressToggleShowHandler.bind(this)}>
                        <Image style={styles.icon} source={require('../../assets/icons/more-options.png')} />
                        <View style={styles.centerBtnShowMore}>
                            <Text style={styles.name}>{isShowMore ? "Ẩn bớt" : "Xem thêm"}</Text>
                        </View>
                        <FontAwesome5Icon style={{ alignContent: 'center' }} name={isShowMore ? "chevron-up" : "chevron-down"} size={20} color="#333" />
                    </TouchableOpacity>
                    <Animated.View style={{ ...styles.moreOptionsWrapper, height: this._moreOptionsHeight, overflow: 'hidden' }}>
                        <ExTouchableOpacity style={styles.btnOption} >
                            <Image style={styles.icon} source={require('../../assets/icons/recommendation.png')} />
                            <View>
                                <Text style={styles.name}>Đề xuất</Text>
                            </View>
                        </ExTouchableOpacity>
                        <ExTouchableOpacity style={styles.btnOption} >
                            <Image style={styles.icon} source={require('../../assets/icons/time.png')} />
                            <View>
                                <Text style={styles.name}>Gần đây nhất</Text>
                            </View>
                        </ExTouchableOpacity>
                        <ExTouchableOpacity style={styles.btnOption} >
                            <Image style={styles.icon} source={require('../../assets/icons/wallet-black-symbol.png')} />
                            <View>
                                <Text style={styles.name}>Facebook Pay</Text>
                            </View>
                        </ExTouchableOpacity>

                        <ExTouchableOpacity style={styles.btnOption} >
                            <Image style={styles.icon} source={require('../../assets/icons/ads.png')} />
                            <View>
                                <Text style={styles.name}>Quảng cáo</Text>
                            </View>
                        </ExTouchableOpacity>
                        <ExTouchableOpacity style={styles.btnOption} >
                            <Image style={styles.icon} source={require('../../assets/icons/cloudy.png')} />
                            <View>
                                <Text style={styles.name}>Thời tiết</Text>
                            </View>
                        </ExTouchableOpacity>
                        <ExTouchableOpacity style={styles.btnOption} >
                            <Image style={styles.icon} source={require('../../assets/icons/wifi-connection.png')} />
                            <View>
                                <Text style={styles.name}>Tìm Wi-Fi</Text>
                            </View>
                        </ExTouchableOpacity>
                        <ExTouchableOpacity style={styles.btnOption} >
                            <Image style={styles.icon} source={require('../../assets/icons/heart1.png')} />
                            <View>
                                <Text style={styles.name}>Chiến dịch gây quỹ</Text>
                            </View>
                        </ExTouchableOpacity>
                        <ExTouchableOpacity style={styles.btnOption} >
                            <Image style={styles.icon} source={require('../../assets/icons/flammable.png')} />
                            <View>
                                <Text style={styles.name}>Ứng phó khẩn cấp</Text>
                            </View>
                        </ExTouchableOpacity>
                        <ExTouchableOpacity style={styles.btnOption} >
                            <Image style={styles.icon} source={require('../../assets/icons/key.png')} />
                            <View>
                                <Text style={styles.name}>Yêu cầu từ thiết bị</Text>
                            </View>
                        </ExTouchableOpacity>
                    </Animated.View>
                    <ExTouchableOpacity style={styles.btnOption} >
                        <Image style={styles.icon} source={require('../../assets/icons/question-mark.png')} />
                        <View>
                            <Text style={styles.name}>Trợ giúp & hỗ trợ</Text>
                        </View>
                    </ExTouchableOpacity>
                    <ExTouchableOpacity style={styles.btnOption} >
                        <Image style={styles.icon} source={require('../../assets/icons/gear.png')} />
                        <View>
                            <Text style={styles.name}>Cài đặt & Riêng tư</Text>
                        </View>
                    </ExTouchableOpacity>
                    <ExTouchableOpacity style={styles.btnOption} onPress={() => this.logout()} >
                        <Image style={styles.icon} source={require('../../assets/icons/logout.png')} />
                        <View>
                            <Text style={styles.name}>Đăng xuất</Text>
                        </View>
                    </ExTouchableOpacity>
                </ScrollView>
            </View>
        )
    }
}
const mapStateToProps = state => {
    return {
        user: state.user.user,
        friends: state.user.friends
    }
}
export default connect(mapStateToProps, null)(index);
const styles = StyleSheet.create({
    container: {
        backgroundColor: 'rgba(0,0,0,0.5)'
    },
    btnProfile: {
        backgroundColor: '#fff',
        flexDirection: 'row',
        alignItems: 'center',
        height: 60,
        paddingHorizontal: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#ddd'
    },
    btnOption: {
        backgroundColor: '#fff',
        flexDirection: 'row',
        alignItems: 'center',
        height: 50,
        paddingHorizontal: 15,
    },
    avatar: {
        height: 32,
        width: 32,
        borderRadius: 32,
        marginRight: 10,
        borderColor: '#333',
        borderWidth: 0.2
    },
    icon: {
        height: 24,
        resizeMode: 'contain',
        marginRight: 10
    },
    name: {
        fontSize: 16,
        fontWeight: 'bold'
    },
    centerBtnShowMore: {
        width: SCREEN_WIDTH - 100
    }
})
