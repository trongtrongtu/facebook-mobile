import React from 'react';
import {StyleSheet, Text, View, TextInput, TouchableOpacity} from "react-native";

export default function CreateAccount({navigation}) {
    return (
        <View style={styles.container}>
            <Text style={styles.title}>Tham gia Facebook</Text>
            <Text style={styles.text}>Chúng tôi sẽ giúp bạn tạo tài khoản mới sau vài bước dễ dàng.</Text>
            <TouchableOpacity style={styles.nextBtn}
                              onPress={() => navigation.navigate('Name')}>
                <Text style={styles.nextText}
                >Tiếp</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.accountBtn}
                              onPress={() => navigation.navigate('Login')}>
                <Text style={styles.accountText}>Bạn đã có tài khoản?</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",
        alignItems: 'center'
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
        marginTop: 90,
        marginBottom: 10
    },
    text: {
        fontSize: 14,
        width: '90%',
        textAlign: 'center',
        color: "#837f7f",
        marginBottom: 20
    },
    nextBtn: {
        width: "90%",
        borderRadius: 5,
        height: 40,
        backgroundColor: "#1877f2",
        alignItems: "center",
        justifyContent: "center",
        marginTop: 80
    },
    nextText: {
        color: "#fff",
        fontSize: 15
    },
    accountBtn: {
        backgroundColor: "#fff",
        justifyContent: 'center',
        height: 50,
        position: 'absolute',
        bottom: 0
    },
    accountText: {
        color: "#1877f2",
        fontWeight: 'bold'
    }
});