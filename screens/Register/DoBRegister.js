import React, { useState, useEffect } from 'react';
import {StyleSheet, Text, View, TextInput, TouchableOpacity} from "react-native";
import {DatePickerView} from "antd-mobile";
import enUs from 'antd-mobile/lib/date-picker-view/locale/en_US';


export default function PasswordRegister({route,navigation}) {
    const maxDate = new Date(2018, 11, 3, 22, 0);
    const minDate = new Date(1950, 7, 6, 8, 30);
    const {ho, ten}=route.params;
    const [value, setValue] = useState(null);
    const onChange = (value) => {
        console.log(value);
        setValue(value);
    };
    const onValueChange = (...args) => {
        console.log(args);
    };
    const checkDate=()=>{

        navigation.navigate('Gender',{ho,ten})
      // console.log('date',value)

    }
    return (
        <View style={styles.container}>
            <Text style={styles.title}>Sinh nhật của bạn khi nào?</Text>
            <DatePickerView
                locale={enUs}
                minDate={minDate}
                maxDate={maxDate}
                mode={"date"}
                value={value}
                onChange={onChange}
                onValueChange={onValueChange}
            />
            <TouchableOpacity style={styles.nextBtn}
                              onPress={() =>checkDate()}>
                <Text style={styles.nextText}>Tiếp</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",
        alignItems: 'center'
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
        marginTop: 90,
        marginBottom: 30
    },
    nextBtn: {
        width: "90%",
        borderRadius: 5,
        height: 40,
        backgroundColor: "#1877f2",
        alignItems: "center",
        justifyContent: "center",
        marginTop: 80
    },
    nextText: {
        color: "#fff",
        fontSize: 15
    }
});
