import React, {Component} from 'react';
import {StyleSheet, TextInput, TouchableOpacity, Text, View} from "react-native";

export default function ForgotPasswordMail({navigation}) {
    return (
        <View style={styles.container}>
            <Text style={styles.title}>Nhập email của bạn</Text>
            <View style={styles.inputView}>
                <TextInput
                    style={styles.inputText}
                    placeholder="Email"
                    placeholderTextColor="#A2A2A2"
                />
            </View>
            <TouchableOpacity style={styles.findBtn}>
                <Text style={styles.findText}>Tìm tài khoản của bạn</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.mailBtn}
                              onPress={() => navigation.navigate('Find your account')}>
                <Text style={styles.mailText}>Tìm kiếm bằng email của bạn</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center'
    },
    title: {
        fontSize: 16,
        marginTop: 30,
        marginBottom: 20
    },
    inputView: {
        width: "90%",
        borderBottomWidth: 2,
        borderBottomColor: "#1877f2",
        marginBottom: 30
    },
    inputText: {
        fontSize: 16
    },
    findBtn: {
        width: "90%",
        backgroundColor: "#1877f2",
        height: 40,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    findText: {
        fontSize: 16,
        fontWeight: 'bold',
        color: "#fff"
    },
    phoneBtn: {
        backgroundColor: "#fff",
        justifyContent: 'center',
        height: 50,
        position: 'absolute',
        bottom: 0
    },
    phoneText: {
        color: "#1877f2",
        fontWeight: 'bold'
    }
});